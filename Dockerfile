FROM d4science/rstudio-base:2023-03-R4-2

LABEL org.d4science.image.licenses="EUPL-1.2" \
      org.d4science.image.source="https://code-repo.d4science.org/gCubeSystem/r-studio-test" \
      org.d4science.image.vendor="D4Science <https://www.d4science.org>" \
      org.d4science.image.authors="Andrea Dell'Amico <andrea.dellamico@isti.cnr.it>, Roberto Cirillo <roberto.cirillo@isti.cnr.it>" \
      org.d4science.image.r_version="2022.02.1+461"

COPY rsession.conf /etc/rstudio/rsession.conf
RUN chmod 0644 /etc/rstudio/rsession.conf
RUN curl -o "${R_HOME}/etc/Rprofile.site" "https://code-repo.d4science.org/gCubeSystem/rstudio-rprofile/raw/branch/master/jupyter-Rprofile.site"

